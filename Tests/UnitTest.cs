using System;
using System.Collections.Generic;
using ConsoleApp1;
using NUnit.Framework;

namespace TestProject1;

public class Tests
{
    private const string FourtyTwo = "the answer to life, universe and everything is 42";
    private static readonly List<QuestionWithAnswer> Questions = new();

    [SetUp]
    public void Setup()
    {
        Questions.Clear();
    }

    [Test]
    public void NoAnswer()
    {
        var output = Program.HandleInput("How are you?", Questions);
        Assert.AreEqual(FourtyTwo, output);
    }

    [Test]
    public void CreateAnswer()
    {
        Program.HandleInput(@"How old are you? ""I'm 21""", Questions);
        var output = Program.HandleInput("How old are you?", Questions);
        Assert.AreEqual("I'm 21\n", output);
    }

    [Test]
    public void CreateAnswers()
    {
        Program.HandleInput(@"What's your favourite food? ""Pizza"" ""Cake"" ""Apples""", Questions);
        var output = Program.HandleInput("What's your favourite food?", Questions);
        Assert.AreEqual("Pizza\nCake\nApples\n", output);
    }

    [Test]
    public void InvalidInput()
    {
        // Missing double quotes at the end
        Assert.Throws<ArgumentException>(
            delegate { Program.HandleInput(@"Ok? ""no", Questions); });

        // Missing question mark
        Assert.Throws<ArgumentException>(
            delegate { Program.HandleInput(@"Ok", Questions); });

        // Empty answer
        Assert.Throws<ArgumentException>(
            delegate { Program.HandleInput(@"Ok? """" ", Questions); });

        // Empty question
        Assert.Throws<ArgumentException>(
            delegate { Program.HandleInput(@"? ""no"" ""never""", Questions); });

        // Two question marks
        Assert.Throws<ArgumentException>(
            delegate { Program.HandleInput("Ok??", Questions); });

        // Question with more than 255 characters
        Assert.Throws<ArgumentException>(
            delegate
            {
                Program.HandleInput(
                    "Okkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk" +
                    "kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk" +
                    @"kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk? ""Maybe""",
                    Questions);
            });
    }
}
