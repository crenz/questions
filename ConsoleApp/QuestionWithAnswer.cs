using System.Text;

namespace ConsoleApp1;

public class QuestionWithAnswer
{
    public string Question { get; }
    public string[] Answers { get; }
    
    public QuestionWithAnswer(string question, string[] answers)
    {
        if (question.Length > 255) throw new ArgumentException("Invalid Question");

        if (answers.Any(antwort => antwort.Length > 255)) throw new ArgumentException("Invalid Answer(s)");

        Question = question;
        Answers = answers;
    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        foreach (var answer in Answers) sb.AppendLine(answer);

        return sb.ToString();
    }
}
