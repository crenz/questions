using System.Text.RegularExpressions;

namespace ConsoleApp1;

public static class Program
{
    private static readonly Regex RgxOnlyQuestion = new(@"^[^?][^?]*\?$");

    private static readonly Regex RgxWithAnswers =
        new(@"^[^?][^?]*\? (\""[^\""][^\""]*\"") ?(\""[^\""][^\""]*\"" ?)*$");

    private static void Main(string[] args)
    {
        var questions = new List<QuestionWithAnswer>();
        while (true)
        {
            Console.Write("> ");
            var input = Console.ReadLine();
            try
            {
                if (input == null) throw new ArgumentException("Invalid input");

                var output = HandleInput(input, questions);
                Console.WriteLine(output);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }

    public static string HandleInput(string input, List<QuestionWithAnswer> questions)
    {
        if (RgxOnlyQuestion.IsMatch(input))
        {
            var foundQuestion = questions.Find(x => String.Equals(x.Question, input));
            return foundQuestion != null
                ? foundQuestion.ToString()
                : "the answer to life, universe and everything is 42";
        }

        if (RgxWithAnswers.IsMatch(input))
        {
            var newQuestion = SplitInput(input);
            questions.Add(newQuestion);
            return "";
        }

        throw new ArgumentException("Invalid input");
    }

    private static QuestionWithAnswer SplitInput(string input)
    {
        var idx = input.IndexOf("?", StringComparison.Ordinal);
        var question = input.Substring(0, idx + 1);
        var answers = new List<string>();
        while (idx < input.Length - 1)
        {
            idx += 3;
            var nextIdx = input.Substring(idx).IndexOf("\"", StringComparison.Ordinal);
            var answer = input.Substring(idx, nextIdx);
            answers.Add(answer);
            idx += nextIdx;
        }

        return new QuestionWithAnswer(question, answers.ToArray());
    }
}
